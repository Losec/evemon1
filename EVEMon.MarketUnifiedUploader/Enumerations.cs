namespace EVEMon.MarketUnifiedUploader
{
    /// <summary>
    /// Enumerations for the uploader status.
    /// </summary>
    public enum UploaderStatus
    {
        Disabled = 0,
        Initializing = 1,
        Idle = 2,
        Uploading = 3,
    }
}